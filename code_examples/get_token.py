def get_token(self, oauth2_session, code, client_id,
              client_secret):
    try:
        token_json = oauth2_session.fetch_token(
            self.path_for_token,
            authorization_response="authorization_code",
            code=code,
            client_id=client_id,
            client_secret=client_secret,
            headers=self.token_headers)

        try:
            # token is OK
            access_token = token_json["access_token"]
            return access_token
        except KeyError:
            try:
                error = token_json["error"]
                OAuthbasedUserManager.logger.error(
                    "Error of access token: %s", error)
            except KeyError:
                OAuthbasedUserManager.logger.error(
                    "Error of access token, "
                    "error message not found")

    except OAuth2Error:
        OAuthbasedUserManager.logger.error(
            "Bad authorization_code")

    return None