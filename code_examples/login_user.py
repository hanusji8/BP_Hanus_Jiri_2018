try:
    redirect_uri = user.get_id()["redirect_uri"]
    code = user.get_id()["code"]
except KeyError:
    OAuthbasedUserManager.logger.error(
        "Code or redirect_uri not found")
    return None

client_id = self.oauth2[redirect_uri]["client_id"]
client_secret = self.oauth2[redirect_uri]["client_secret"]
oauth2_session = OAuth2Session(client_id,
                               redirect_uri=redirect_uri)
access_token = self.get_token(oauth2_session, code,
                              client_id, client_secret)

if access_token is None:
    return None

username = self.get_username(oauth2_session)
if username is None:
    OAuthbasedUserManager.logger.error("Username none")
    return None
user = FilebasedUserManager.findUser(self, username)

if user is None:
    # addUser(username, password, active, roles)
    # password not needed, user will be active, role is user
    self.addUser(username, "", True, ["user"])
    user = self.findUser(username)