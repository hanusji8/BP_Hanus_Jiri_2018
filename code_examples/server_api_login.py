user = octoprint.server.userManager.findUser(username)
if user is not None:
  if octoprint.server.userManager.checkPassword(username,
                                                password):
    if not user.is_active():
      return make_response(("Your account is deactivated",
                            403, []))

    if octoprint.server.userManager.enabled:
      user = octoprint.server.userManager.login_user(user)
      session["usersession.id"] = user.session
      g.user = user
    login_user(user, remember=remember)
    identity_changed.send(current_app._get_current_object(),
                          identity=Identity(user.get_id()))
    return jsonify(user.asDict())
return make_response(("User unknown or password incorrect",
                      401, []))