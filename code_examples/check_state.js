const code = getParameterByName("code", window.location.href);
const stateFromOAuth = getParameterByName("state",
    window.location.href);

if(!!stateFromOAuth && !!code){
    const state = localStorage.getItem("state");
    localStorage.removeItem("state");
    if (stateFromOAuth != state) {
        alert("State sent to oauth server is not same." +
             " Possible attack");
        return;
    }

    const url = parseUrl(window.location.href).origin + "/";
    const parameters = {"code": code,
                        "redirect_uri": url};

    OctoPrint.browser.login(parameters, "unused", false)