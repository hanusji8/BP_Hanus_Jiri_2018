def findUser(self, userid=None, apikey=None, session=None):
    user = FilebasedUserManager.findUser(self, userid,
                                         apikey, session)
    if user is not None:
        return user
    
    user = User(userid, "", 1, ["user"])
    return user