\section{OAuth 2.0}
OAuth 2.0 \cite{oauth2} je autorizační framework umožnující aplikacím třetích stran obdržet omezený přístup k~nějaké webové službě. Tento framework (resp. protokol) pro autorizaci nahrazuje již zastaralý OAuth 1.0 \cite{oauth1} protokol z~roku 2006. Hlavní výhoda OAuth 2.0 (dále jen OAuth2) je taková, že uživatel může poskytnout klientské aplikaci přístup ke svým datům z~nějakého serveru, aniž by klientské aplikaci musel poskytnout své přihlašovací údaje.

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\textwidth]{img/oauth_general.pdf}
    \caption{Proces autorizace použitím protokolu OAuth 2.0}
    \label{fig:oauth_general}
\end{figure}

\subsection{Základní principy}
V~klasickém modelu klient-server klient žádá o~data ze serveru, kde je omezený přístup a je potřeba autorizace. To přináší několik problémů, jako kde si bude klientská aplikace ukládat hesla uživatelů? Jak servery, kde jsou informace uloženy, zjistí, že informace dává správnému klientovi?

OAuth2 tohle řeší tak, že se nepřihlašuje přímo ke zdroji informací, ale nejdříve k~autorizačnímu serveru, kde se koncový uživatel přihlásí. Tím dá vědět serveru, že může poskytnout jeho informace. Autorizační server ale nemá k~informacím o~uživateli přístup. Tento server pošle zpátky klientské aplikaci přístupový token, díky kterému uživatel může skrz klientskou aplikaci přistoupit ke svým datům na zdrojovém serveru. Na úvod je potřeba vysvětlit několik základních rolí.

\begin{itemize}
   \item První z~nich je takzvaný majitel zdroje. Tento majitel (dále jako koncový uživatel) má někde uchovaná svá data, která by chtěl poskytnout klientské aplikaci. Majitel zdroje musí dát klientské aplikaci povolení (grant), který pak může klientská aplikace použít. Existuje několik typů grantů, které popíšu v~podsekci \ref{oauth:autorizacni_granty} -- Autorizační granty.

   \item Další z~důležitých rolí je server, na kterém jsou data o~uživateli uchována (dále jako zdrojový server). Ke zdrojovému serveru přes OAuth2 nelze přistoupit přímo. Aby klient mohl přistoupit k~datům, potřebuje přístupový token z~autorizačního serveru.

   \item Třetí z~rolí je klient. Uživatel se skrze klienta (resp. klientskou aplikaci) pomocí autorizačního povolení přihlašuje k~autorizačnímu serveru, aby obdržel přístupový token. Díky tomuto tokenu si klientská aplikace může vyzvednout data ze zdrojového serveru.

   \item Poslední z~rolí, které si nadefinujeme, je autorizační server, na kterém se uživatel přihlásí, dá vědět, jaká data má zdrojový server poskytnout. Autorizační server pak vytvoří přístupový token.

\end{itemize}

\subsection{Autorizační granty}\label{oauth:autorizacni_granty}
Autorizační grant je pověření od koncového uživatele pro klientskou aplikaci. Tento grant se využívá pro získání přístupového tokenu od autorizačního serveru. Existují čtyři druhy autorizačních grantů. Autorizační kód, implicitní grant, přístupové údaje majitele zdroje, klientské pověření.

\begin{itemize}
    \item Autorizační kód je typ pověření od koncového uživatele. Tento je obdržen od serveru služby po přihlášení uživatele. Klientská aplikace poté může tento kód použít na autorizačním serveru, aby obdržela přístupový token. Server, na kterém se uživatel přihlásil, předá tuto informaci autorizačnímu serveru. Díky tomu klientská aplikace vůbec nepotřebuje znát přihlašovací údaje koncového uživatele.

    \item Implicitní grant je zjednodušení verze autorizačního kódu. Místo toho, aby ale klient dostal pouze kód, obdrží přímo přístupový token. V~tomto případě se už není potřeba identifikovat na autorizačním serveru. Implicitní granty sice mohou přinést větší efektivitu pro nějaké klientské aplikace, ale velmi to snižuje bezpečnost dat.

    \item Třetí z~možnosti je přímé získání přístupových údajů uživatele jako je přihlašovací jméno a heslo. Tento způsob pověření by měl být použit pouze v~případě velké důvěry mezi klientskou aplikací a koncovým uživatelem.

    \item Klientské pověření je typ grantu, který se používá, když je určen nějaký rámec poskytnutých dat ze zdrojového serveru. V~případě tohoto pověření má klientská aplikace stejné možnosti jako samotný koncový uživatel. Tedy aplikace může přistupovat ke zdroji informací.
\end{itemize}

\subsection{Klient a servery}\label{oauth:klient_a_servery}
Před používáním protokolu OAuth2 je potřeba si vytvořit klientskou aplikaci na autorizačním serveru. Každá klientská aplikace se vyznačuje třemi základními prvky. Jsou to \texttt{client\_id}, \texttt{client\_secret} a \texttt{redirect\_uri}. Indetifikátor klientské aplikace \texttt{client\_id} je unikátní řetězec pro každý klient na serveru služby. Druhý prvek je \texttt{client\_secret}, které slouží jako heslo klientské aplikace. Třetí hodnota je přesměrovací adresa zpět do aplikace, ze které se autorizace provádí.

Pro OAuth2 se používají dva hlavní servery. Jedním z~nich je autorizační server, na který se klientská aplikace přihlásí pomocí pověření od koncového uživatele. Ten poté předá zpátky aplikaci přístupový token, nebo v~případě špatné autorizace předá chybový kód a informaci o chybě. Druhý je zdrojový server, na kterém jsou uloženy informace o~koncovém uživateli, které mohou být důvěrné.

\subsection{Typy tokenů}
Existují dva tokeny, které se využívají při autorizaci pomocí OAuth2 -- přístupový token a obnovovací token.

Přístupové tokeny jsou způsob, jakým jsou uloženy přístupové údaje, aby se aplikace dostala k~chráněným informacím. Obvykle je to náhodný řetězec vygenerovaný autorizačním serverem. Tento řetězec je pro klientskou aplikaci nesrozumitelný. Přístupový token v~sobě může obsahovat informaci, jak hodně omezené informace má zdrojový server poskytnout. Přístupový token nelze používat do nekonečna. Váže se k~němu i volitelná informace o~tom, kdy daný token už nebude platit.

Obnovovací tokeny se používají k~tomu, aby klientská aplikace mohla obdržet od autorizačního serveru nový přístupový token, když ten starý přestane platit. I~tento obnovovací token je řetězec, který je pro klientskou aplikaci nečitelný. Obnovovací token, narozdíl od přístupového, se ale nedostane do styku se zdrojovým serverem.


\subsection{Přístupový token}
V~případě, že naše aplikace potřebuje přístupový token, pošle na autorizační server grant od koncového uživatele.
Jsou dvě odpovědi, které aplikace od autorizačního serveru dostat -- úspěch či chyba.

V~případě úspěchu dostane také od serveru přístupový token. Dále obdrží jeden z~typů tokenu, v~současné době existují dva: bearer \cite{bearer} a mac \cite{mac}. Dále může aplikace získat dobu, za jakou platnost tokenu vyprší. Tato hodnota je doporučovaná. 

V~případě neúspěchu odezvy můžeme obdržet několik možností napovídající chybě. Chyba \texttt{invalid\_request} může značit chybějící požadovaný parametr, nebo například že se jeden parametr objevil dvakrát. Další z~možností je
\texttt{invalid\_client}, který značí to, že bylo špatně zadáno \texttt{client\_id} nebo \texttt{client\_secret} během autorizace. Další eventualita je \texttt{invalid\_grant} uvádějící špatně použitý grant. Tato chyba například nastane, pokud se použije špatný autorizační kód \cite{oauth2}. 


\subsection{Aplikace využívající OAuth 2.0}
Na závěr této sekce bych rád zmínil několik aplikací, pomocí nichž je možné použít protokol OAuth 2.0. Jsou to často sociální sítě jako Facebook, Instagram, Twitter nebo LinkedIn \cite{oauth_examples}. Dále se dá využít tento způsob autorizace přes Google. Další možností je využít OAuth2 přes GitHub, kde je poměrně jednoduché si vytvořit vlastní klientskou aplikaci. Jako poslední bych zmínil ČVUT v~Praze, kde je možné si také vytvořit vlastní klientské aplikace a využívat je s~autorizačním serverem na FIT ČVUT v~Praze \cite{rozvoj_oauth}. V kombinaci s KOSapi \cite{kosapi} se dají pak dělat webové aplikace jako je například 730ne.cz \cite{730ne}.